///////////////variables an that///////////////

window.addEventListener("keydown", function(e) {
    if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
        e.preventDefault();
    }
}, false);

const canvas = document.getElementById("d_canvas")
const context = canvas.getContext("2d");

let username = localStorage.getItem('username');
const score = localStorage.getItem('score');

const scale = 5;
const width = 17;
const height = 17;
const scaledWidth = scale * width;
const scaledHeight = scale * height;
const walkLoop = [0, 1, 1, 0, 0];
const frameLimit = 16;
const foodWidth = 32;
const foodHeight = 32;
const secondImages = 3;
const startImage2 = width * secondImages;



let currentLoopIndex = 0;
let frameCount = 0;
let currentDirection = 0;
let speed = 1.5;
let randomX = Math.abs(Math.floor(Math.random() * 1099) - 50);
let randomY = Math.abs(Math.floor(Math.random() * 499) - 50);
let randomFoodX = Math.abs(Math.floor(Math.random() * 7));
let randomFoodY = Math.abs(Math.floor(Math.random() * 4));
let randomswanX = Math.abs(Math.floor(Math.random() * 7));
let randomswanY = Math.abs(Math.floor(Math.random() * 4));

let gamesEnd = false;
let gamesWins = false;

let scoreCount = 0;
if (score)
{
    scoreCount = score;
}

function randoPos(rangeX, rangeY, delta)
{
    this.x = Math.abs(Math.floor(Math.random() * rangeX) - delta);
    this.y = Math.abs(Math.floor(Math.random() * rangeY) - delta);
}

function newFood()
{
    randomFoodX = Math.abs(Math.floor(Math.random() * 7));
    randomFoodXSelect = randomFoodX * 64;
    randomFoodY = Math.abs(Math.floor(Math.random() * 4));
    randomFoodYSelect = randomFoodY * 64;
    foodPosition = new randoPos(999, 499, 50);
}



function newSwanDes(){
    randomswanX = (Math.floor(Math.random() * 500) +500);
    randomswanY = (Math.floor(Math.random() * 500));
}
///////////////variables an that///////////////
///////////////player and food stuff///////////////
let background = new Image();
background.src = "assets/media/bg.png"

let playerImage = new Image();
playerImage.src = "assets/media/black-mage-overworld-sprite.png";

let foodSprite = new Image();
foodSprite.src = "assets/media/fruit.png";

let enemySprite = new Image();
enemySprite.src = "assets/media/plz.jfif";

let enemy2Sprite = new Image();
enemy2Sprite.src = "assets/media/goose.jpg";

let endScreen = new Image();
endScreen.src = "assets/media/lose.png";

let winScreen = new Image();
winScreen.src = "assets/media/win.png";




function drawFrame(playerImage, frameX, frameY, canvasX, canvasY)
{
    context.drawImage(playerImage,
                  frameX * width, frameY * height, width, height,
                  canvasX, canvasY, scaledWidth, scaledHeight);
}

function addName() 
{
    let header = document.getElementById("main-header");
    header.innerHTML = "Hello " + username;
}

addName();

// GameObject holds positional information
function GameObject(spritesheet, x, y, width, height) 
{
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.mvmtDirection = "None";
}

// Default Player
let bg = new GameObject(background, 0, 0, 1000, 500);
let player = new GameObject(playerImage, 0, 0, 200, 200);
let foods = new GameObject(foodSprite, randomX, randomY, 100, 100);
let enemy = new GameObject(enemySprite, 1000, 20, 200, 200);
let enemy2 = new GameObject(enemy2Sprite, 900, 300, 200, 200);
let gameOver = new GameObject(endScreen, 0, 0, 1500, 500);
let win = new GameObject(winScreen, 0, 0, 1000, 500);

// The GamerInput is an Object that holds the Current
// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) 
{
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input

///////////////player and food stuff///////////
///////////////movement///////////////

// Take Input from the Player
function input(event) 
{
    
    if (event.type === "keydown") {
        switch (event.keyCode) {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break;
            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break;
            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break;
            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break;
            case 49: //Number 1
                gamerInput = new GamerInput("1");
            break; 
            case 50: //Number 2 
                gamerInput = new GamerInput("2");
            break;
            case 51: //Number 3
                gamerInput = new GamerInput("3");
            break;
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None");
    }
}

function update() 
{



    // Move Player Up
    if (gamerInput.action === "Up") {
        console.log("Move Up");
        player.y -= speed;
        currentDirection = 2;
    }
    // Move Player Down
    else if (gamerInput.action === "Down") {
        console.log("Move Down");
        player.y += speed;
        currentDirection = 0; 
    }
    // Move Player Left 
    else if (gamerInput.action === "Left") {
        console.log("Move Left");
        player.x -= speed;
        currentDirection = 1; 
    }
    // Move Player Right
    else if (gamerInput.action === "Right") {
        console.log("Move Right");
        player.x += speed;
        currentDirection = 3; 
    }

}

var dynamic = nipplejs.create
({
    color: 'pink',
    zone: document.getElementById("d_canvas")
});

dynamic.on('added', function (evt, nipple) 
{
    //nipple.on('start move end dir plain', function (evt) {
    nipple.on('dir:up', function (evt, data) {
       //console.log("direction up");
       gamerInput = new GamerInput("Up");
    });
    nipple.on('dir:down', function (evt, data) {
        //console.log("direction down");
        gamerInput = new GamerInput("Down");
     });
     nipple.on('dir:left', function (evt, data) {
        //console.log("direction left");
        gamerInput = new GamerInput("Left");
     });
     nipple.on('dir:right', function (evt, data) {
        //console.log("direction right");
        gamerInput = new GamerInput("Right");
     });
     nipple.on('end', function (evt, data) {
        //console.log("mvmt stopped");
        gamerInput = new GamerInput("None");
     });
});

function moveEnemy(){
    if(player.x < enemy.x){
        enemy.x -= .5; //move enemy left
    }
    
    if(player.x > enemy.x){
        enemy.x += .5;//move enemy right
    }

    if(player.y < enemy.y){
        enemy.y -= .5;//move enemy up
    }

    if(player.y > enemy.y){
        enemy.y += .5;//move enemy down
    }
}


///////////////movement///////////////
///////////////animation score and food//////////////


function animate() 
{
    if (gamerInput.action != "None"){
        frameCount++;
        if (frameCount >= frameLimit) {
            frameCount = 0;
            currentLoopIndex++;
            if (currentLoopIndex >= walkLoop.length) {
                currentLoopIndex = 0;
            }
        }      
    }
    else{
        currentLoopIndex = 0;
    }
    drawFrame(player.spritesheet, walkLoop[currentLoopIndex], currentDirection, player.x, player.y);
}

foodPosition = new randoPos(1099, 499, 50);

function manageFood()
{
    // place the piece of food
    context.drawImage(foodSprite, randomFoodX*64, randomFoodY*64, 64, 64, foodPosition.x, foodPosition.y, foodWidth, foodHeight);
    // check for collision (eating)

    //the numbers are the sizes if the sprites
    if (player.x < foodPosition.x && (player.x + 64) > (foodPosition.x + 32) && player.y < foodPosition.y && (player.y + 64) > (foodPosition.y + 32)) 
    {
        console.log(" collision !");
        scoreCount ++;
        localStorage.setItem("score", scoreCount);
        newFood();
    }
}

function writeScore()
{
    let scoreString = "score: " + scoreCount;
    context.font = "22px sans-serif";
    context.fillStyle = "pink";
    context.fillText(scoreString, 890, 60);
}

function lakeColitions(){

    if (player.x < foodPosition.x && (player.x + 64) > (foodPosition.x + 32) && player.y < foodPosition.y && (player.y + 64) > (foodPosition.y + 32)) 
    {
        console.log(" collision !");
        scoreCount ++;
        localStorage.setItem("score", scoreCount);
        newFood();
    }

}

function goosecol(){
    context.drawImage(enemy.spritesheet, startImage2 + width, 
        0, width, height, enemy.x, enemy.y, width, height);

        if(enemy.x < player.x + width && enemy.x + width > player.x
            && enemy.y < player.y + height && enemy.y + height > player.y){
            
                console.log("collision1111111111");
                gamesEnd = true;
            }
    
}

function swancol(){
    context.drawImage(enemy2.spritesheet, startImage2 + width, 
        0, width, height, randomswanX, randomswanY, width, height);

        if (player.x < enemy2.x && (player.x + 64) > (enemy2.x + 32) && player.y < enemy2.y && (player.y + 64) > (enemy2.y + 32)) 
        {
            
                console.log("collision22222222");
                gamesEnd = true;
            
        }

}



///////////////animation score and food//////////////
///////////////important stuff dont touch//////////////

setInterval(newSwanDes, 5000);

function draw() 
{


    // Clear Canvas
    context.clearRect(0, 0, canvas.width, canvas.height);

    context.fillRect(0, 0, 100, 500);

    context.drawImage(bg.spritesheet, bg.x, bg.y, 1000, 500);

    if(scoreCount >= 5)
    {
        if(player.x >= 0 && player.x <= 100)
        {
            context.drawImage(win.spritesheet, win.x, win.y, 1000, 500);

            //scoreCount = 0;
        }
    }



    
    manageFood();
    goosecol();
    swancol();
    animate();
    writeScore();

    /* if (!gameWins) {
        animate();
        writeScore();
    } else {
        context.drawImage(win.spritesheet, win.x, win.y, 800, 500);
    }*/


    if (!gamesEnd) {
        animate();
        writeScore();
    } else {
        context.drawImage(gameOver.spritesheet, gameOver.x, gameOver.y, 1000, 500);
        scoreCount = 0;

    }


}

function gameloop() 
{
    update();
    draw();
    moveEnemy();
    window.requestAnimationFrame(gameloop);
}

// Handle Active Browser Tag Animation
window.requestAnimationFrame(gameloop);

window.addEventListener('keydown', input);

// disable the second event listener if you want continuous movement
window.addEventListener('keyup', input);
